# ANGULAR IMAGE FOR THE MEETUP SO WE DON'T HAVE TO WAIT FOR THE ANGULAR-CLI AND LIVE-SERVER TO INSTALL
# ALSO, MAKE SURE THE CORRECT RIGHTS ARE SET
FROM node:9.11.1-alpine

LABEL maintainer="Niek Heezemans - niek@frontmen.nl"

# SERVER SPECIFIC
USER root

# APP SPECIFIC CONFIGURATION
USER node
RUN mkdir /home/node/.npm-global
ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

# INSTALL ANGULAR/CLI
RUN npm install -g node-sass
RUN npm install -g @angular/cli
RUN npm install -g live-server

# CHANGE THE WORKDIR
WORKDIR /var/nodejs/app

# SET PERMISSIONS
USER root
RUN chown -R node:node .

# CHANGE USER TO BODE
USER node

# BLANK ENTRYPOINT
ENTRYPOINT []

# NO START COMMAND
CMD []

